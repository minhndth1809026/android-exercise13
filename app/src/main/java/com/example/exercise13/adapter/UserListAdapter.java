package com.example.exercise13.adapter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.TextView;

import com.example.exercise13.R;
import com.example.exercise13.activities.EditActivity;
import com.example.exercise13.entity.User;
import com.example.exercise13.interfaces.IOnClick;

import java.util.List;

public class UserListAdapter extends BaseAdapter {
    private final Activity activity;
    private final List<User> users;
    private IOnClick iOnClick;

    public UserListAdapter(Activity activity, List<User> users) {
        this.activity = activity;
        this.users = users;
    }

    public void setiOnClick(IOnClick iOnClick) {
        this.iOnClick = iOnClick;
    }

    @Override
    public int getCount() {
        return users.size();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if(convertView == null) {
            LayoutInflater inflater = activity.getLayoutInflater();
            convertView = inflater.inflate(R.layout.activity_item, null);
            ViewHolder holder = new ViewHolder();
            holder.tvUsername = convertView.findViewById(R.id.tvUsername);
            holder.tvPassword = convertView.findViewById(R.id.tvPassword);
            holder.btEdit = convertView.findViewById(R.id.btEdit);
            holder.btDelete = convertView.findViewById(R.id.btDelete);
            convertView.setTag(holder);
        }

        ViewHolder holder = (ViewHolder) convertView.getTag();
        holder.tvUsername.setText(users.get(position).getUsername());
        holder.tvPassword.setText(users.get(position).getPassword());

        holder.btEdit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onEdit(position);
            }
        });
        
        holder.btDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onDelete(position);
            }
        });

        return convertView;
    }

    private void onDelete(int position) {
        User user = users.get(position);
    }

    private void onEdit(int position) {
        User user = users.get(position);
        Intent intent = new Intent(activity, EditActivity.class);
        intent.putExtra("id", user.getId());
        intent.putExtra("username", user.getUsername());
        intent.putExtra("password", user.getPassword());
        activity.startActivity(intent);
    }

    static class ViewHolder {
        TextView tvUsername;
        TextView tvPassword;
        Button btEdit;
        Button btDelete;
    }
}
