package com.example.exercise13.activities;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.example.exercise13.R;
import com.example.exercise13.adapter.UserListAdapter;
import com.example.exercise13.entity.User;
import com.example.exercise13.network.APIManager;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class EditActivity extends AppCompatActivity {

    EditText etEditUsername;
    Button btEditSave;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit);
        etEditUsername = findViewById(R.id.etEditUsername);
        btEditSave = findViewById(R.id.btEditSave);

        Intent intent = getIntent();
        etEditUsername.setText(intent.getStringExtra("username"));
        int id = intent.getIntExtra("id", 0);
        String password = intent.getStringExtra("password");

        btEditSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                save(id, new User(id, etEditUsername.toString(), password));
            }
        });
    }

    private void save(int id, User user) {
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(APIManager.URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        APIManager service = retrofit.create(APIManager.class);
        service.editUser(id, user);
    }
}