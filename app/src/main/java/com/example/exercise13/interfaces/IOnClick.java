package com.example.exercise13.interfaces;

public interface IOnClick {
    void onClickItem(int position);
}
