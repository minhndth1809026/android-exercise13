package com.example.exercise13.network;

import com.example.exercise13.entity.User;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.DELETE;
import retrofit2.http.Field;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.PUT;

public interface APIManager {
    String URL = "https://api-finalpj.herokuapp.com/v1/user/";

    @GET("list")
    Call<List<User>> getAllUser();

    @GET("view/")
    Call<User> getUser(@Field("id") int id);

    @POST("save")
    void insertUser(@Body User user);

    @PUT("edit/")
    void editUser(@Field("id") int id, @Body User user);

    @DELETE("delete/")
    void deleteUser(@Field("id") int id);
}
